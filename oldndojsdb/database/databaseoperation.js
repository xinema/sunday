/* author: Quoc */
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";
module.exports = {


  getboardbyuserid : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID('59d006e2519079218886a231vvd');
      return db.collection('boards').find( {'user_id': o_id } ).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },


  getboardbykeyold : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find( {'_id': o_id } ).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },




  getboardbykey : function(boardid)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(boardid);
      
      return db.collection('boards').find( {} , {'_id': o_id } ).toArray();  

      //return db.collection('boards').find().toArray();

      //return db.collection('boards').find( { }  , { '_id': true }).toArray();

      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },




  getboardValuesandNamebykey : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find(  {'_id': o_id }, {'boardvalues': true, 'username.fullname' : true }   ).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },





  getboardvaluesbykey : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find(  {'_id': o_id }, {'boardvalues': true} ).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },


  getboardvaluesbykey : function(user)
  {
     return MongoClient.connect(url).then(function (db) {
      var ObjectID = require('mongodb').ObjectID;
      var o_id = new ObjectID(user);
      return db.collection('boards').find(  {'_id': o_id }, {'boardvalues': true} ).toArray();  
      }).then(function(items) {
      console.log(items);
      return items;
      });      
  },

  boards : function()
  {        
     return  MongoClient.connect(url).then(function (db) {          
      var collection = db.collection('boards'); 

      
      return collection.find( {  }  , {'boardvalues' : true, 'boardname' : true  , 'username.fullname' : true,   'username._id' : true } ,  ).toArray();


     /// return collection.find( {  } ,   { 'username.fullname' : { 'username.fullname' : true } } ).toArray();


     //return collection.find( {  } ,   { 'username.fullname' : '_id' } ).toArray();


    //dum//return collection.find( {  } ,   { 'username.fullname' : { $elemMatch: { '_id' : true } } } ).toArray();


    // return collection.find(   { 'username.1'  :  { fullname : true } } ).toArray();
      
     //return collection.find().toArray();

    //   return db.boards.aggregate(
    //     [
    //         { "$project": { "matched": { "$arrayElemAt": [ "$username", 1 ] } } } 
    //     ] 
    // )


      }).then(function(items) {
      console.log(items);
      return items;
      });
    },




    addnotes : function(boards)
    {
        return  MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          //var o_id = new ObjectID(user);
  
          //var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(boards.boardkey);
  
          var myobj = {'boardkey': o_id, 'notename': boards.notename,
          'notes': boards.notes};

          db.collection("notes").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("document inserted");
            db.close();               
          });
        });
      },

  addboard : function(boards)
  {
      return  MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        //var o_id = new ObjectID(user);

        //var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boards.userkey);

        var myobj = {'boardname': boards.boardname, 'boardvalues': boards.boardv,
        'user_id': o_id};
        db.collection("boards").insertOne(myobj, function(err, res) {
          if (err) throw err;
          console.log("document inserted");
          db.close();               
        });
      });
    },

    addboard2 : function(boards)
    {
      return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boards.userkey);
        return db.collection('users').find( {'_id': o_id } , {'fullname': true } ).toArray();  
        }).then(function(items) {
          
          
         MongoClient.connect(url).then(  function(db) {
        
          var myobj = {'boardname': boards.boardname, 'boardvalues': boards.boardv,
          'username': items};

        return  db.collection("boards").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("document inserted");
            db.close();               
          });

        //});
          
      })});

    },
  
   
   userslist : function()
    {        
       return  MongoClient.connect(url).then(function (db) {          
        var collection = db.collection('users');    
        return collection.find( { }  , { 'fullname': true }).toArray();
        }).then(function(items) {
        console.log(items);
        return items;
        });
    },
    adduser : function(user)
    {
        return  MongoClient.connect(url).then(function (db) {
          var myobj = {'fullname': user.fullname, 'username': user.email, 'userrole': user.role };
          db.collection("users").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("document inserted");
            db.close();               
          });
        });
      },
      getuserbykey : function(user)
      {
         return MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(user);
          return db.collection('User').find( {'_id': o_id }).toArray();  
          }).then(function(items) {
          console.log(items);
          return items;
          });      
      },
      changeuserdata : function(user)
      {                  
        return MongoClient.connect(url).then(function (db) {   
            var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(user._id);
            db.collection('User').update({_id: o_id} , { $set : 
                { 
                'fullname': user.fullname,
                'email': user.email,
                'role': user.userrole
                } 
            }, function(err, res) {             
              if (err) throw err;
              console.log("document cannot updated");
              db.close();     
            });
           });
       },
       deleteuser : function(user)
       {
        return  MongoClient.connect(url).then(function (db) {
            var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(user);
            db.collection("User").remove({_id: o_id }, function(err, res) {
              if (err) throw err;
              console.log("document deleted");
              db.close();               
            });
          });
       },


       getnotesbyboardkey: function(boardkey)
       {
       return MongoClient.connect(url).then(function (db) {
        var ObjectID = require('mongodb').ObjectID;
        var o_id = new ObjectID(boardkey);
        return db.collection('notes').find(  {'boardkey': o_id } , {'notename': true  },  {'notes' : true }).toArray();  
        }).then(function(items) {
        console.log(items);
        return items;
        });
      },


      
        getnotesbykey :  function(key)
        {          
        return MongoClient.connect(url).then(function (db) {
          var ObjectID = require('mongodb').ObjectID;
          var o_id = new ObjectID(key);

          //var o_id = new ObjectID('59d8870bb0b8d826f0f32641');

          //return db.collection('notes').find(  {'_id': o_id } , {'notename': true }, {'notes': true }).toArray();
          
          return db.collection('notes').find(   {'_id': o_id } , { 'notename': true , 'notes': true }).toArray();

          }).then(function(items) {
          console.log(items);
          return items;
          });
        },


        changenotes : function(noteinfo)
        {  
           return MongoClient.connect(url).then(function (db) {   
            var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(noteinfo.key);

            var myquery = { _id: o_id   } ;
            var newvalues = { $set: { 'notename': noteinfo.notename, 'notes': noteinfo.notes} };

            db.collection('notes').update(myquery , newvalues
            , function(err, res) {             
              if (err) throw err;
              console.log("document is updated");
              db.close();     
            });
           });    
        },


        deletenotes : function(key)
        {
          return  MongoClient.connect(url).then(function (db) {
            var ObjectID = require('mongodb').ObjectID;
            var o_id = new ObjectID(key);
            db.collection("notes").remove({_id: o_id }, function(err, res) {
              if (err) throw err;
              console.log("document deleted");
              db.close();               
            });
          });
        },      
    } 

    
  