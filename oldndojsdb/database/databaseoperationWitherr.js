// /* author: Quoc */
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/mydb";

module.exports = {
    userslist : function()
    {        
       return  MongoClient.connect(url).then(function (db) {          
        var collection = db.collection('User');    
        return collection.find().toArray();
        }).then(function(items) {
        console.log(items);
        return items;
        });
    },
    adduser : function(user)
    {
        return  MongoClient.connect(url).then(function (db) {
          var myobj = {'fullname': user.fullname, 'username': user.username, 'email': user.email };
          db.collection("User").insertOne(myobj, function(err, res) {
            if (err) throw err;
            console.log("1 document inserted");
            db.close();               
          });
        });
      },


      getuserbykey : function(user)
      {
         return MongoClient.connect(url).then(function (db) {
          //if (err) throw err;
          //var userid = {'_id': user._id };    
          //var ObjectId = new db.ObjectId('59d006e2519079218886a231');    
          //var ObjectId = require('mongodb').ObjectId; 
          //var id = userid;
          //var o_id = new ObjectId(id);
           //db.test.find({_id:o_id})
          //ObjectID.createFromHexString(req.params.id)
          
          // work codes
          var ObjectID = require('mongodb').ObjectID;

          var o_id = new ObjectID(user);
          //var o_id = new ObjectID("59d006e2519079218886a231");         
          //collection.update({'_id': o_id});

          return db.collection('User').find( {'_id': o_id } , {'fullname': true } ).toArray();

          }).then(function(items) {
          console.log(items);
          return items;
          });
      },
};
  