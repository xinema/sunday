/* author: Quoc */
var express = require('express');
var router = express.Router();

var passport = require('passport');

var database = require('../database/databaseoperation.js');
router.get('/',  isLoggedIn, function(req, res, next) {
  database.boards().then(
    function (items) {
      console.log(items);
      res.render('boards', { "boards": items , user: req.user });
    }
    );
});


// router.get('/:key/boards', function (req, res) {
//   var userkey = req.params.userid;
//   database.getboardsbyuserid(userkey)
//     .then(function (items) {
//       console.log(items);
//       res.render('boards', { "boards": items });
//     },
//       function (err) {
//         console.error('Problem .............', err);
//         res.send("Problem collecting data from the database. " + err);
//       }
//       );
// });

// router.get('/:key', function (req, res) {
//   database.boards().then(
//     function (items) {
//       console.log(items);
//       res.render('boards', { "boards": items });
//     }
//     );

  // var userkey = req.params.userid;
  // database.getboardsbyuserid(userkey)
  //   .then(function (items) {
  //     console.log(items);
  //     res.render('boards', { "boards": items });
  //   },
  //     function (err) {
  //       console.error('Problem .............', err);
  //       res.send("Problem collecting data from the database. " + err);
  //     }
  //     );
//});

router.get('/:key/newboard', function (req, res) {
  res.render('addboard', { title: 'Add new board' });
});
router.get('/:key/addnotes', function (req, res) {
  res.render('addnotes', { title: 'Add new notes' });
});



router.get('/:key/getnotes', function (req, res) {
  var boardkey = req.params.key;
  database.getnotesbyboardkey(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('boardnotes', { "notes": items });
    },
      function (err) {
        console.error('Problem ....:', err);
        res.send("problem with database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key/update', function (req, res) {
  var boardkey = req.params._id;
  database.getnotesbykey(boardkey) 
    .then(function (list) {
      console.log(list);
      res.render('changenotes', { "notes": list[0] });
    },
      function (err) {
        console.error('Problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.post('/:key/notesinfo/:key/update', function (req, res) {
  var noteinfo = {
    "key": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes
  };
  database.changenotes(noteinfo)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('Problem .....:', err);
        res.send("problem with the database. " + err);
      }
      );
});
router.get('/:key/notesinfo/:key/delete', function (req, res) {
  var userkey = req.params._id;
  database.deletenotes(userkey)
    .then(function (list) {
      console.log(list);
      res.redirect("/boards");
    },
      function (err) {
        console.error('problem ......:', err);
        res.send("problem with the database. " + err);
      }
      );
});


  router.get('/:key/notesinfo/:key', function (req, res) {
  var notekey = req.params.key;

  // var notekey = {   
  //   "notekey": req.body.notes_id
  // };

 // var notekey = req.body.notes_id

  database.getnotesbykey(notekey)
    .then(function (items) {
      console.log(items);
      res.render('notesdata', { "notes": items[0] });
    },
      function (err) {
        console.error('problem .......', err);
        res.send("problem with the database. " + err);
      }

      );
});


router.post('/:key/addboard', function (req, res) { 
  // var user = {
  //   "username": req.body.username,
  //   "email": req.body.useremail
  // };
  var board = {
    "userkey": req.params.key,
    "boardname": req.body.boardname,
    "boardv": req.body.v2
    //"boardv": req.body.xx    ? true : false,
  };
    database.addboard2(board)
    .then(
      function (result) { console.log(result); res.redirect("/boards"); },
      function (err) {
        console.error('problem .........:', err);
        res.send("problem with the database. " + err);
      }
      );
});



// router.get('/:key', passport.authenticate('local-login', {session: false}), function (req, res) {
  
//   if (req.user.role == "Admin") {
//     return res.send('you can see this content');
// }
// else {
//     return res.send('you can not see this content');
// }

router.get('/:key', function (req, res) {
  
  // if (req.user.role == "Admin") {
  //   //return res.send('you can see this content');
  //   else {
  //     return res.send('you can not see this content');
  //  }
  
  var boardkey = req.params.key;

  database.getboardbykey(boardkey)
    .then(function (list) {
      console.log(list);
  
      ////////  old
      // database.getboardvaluesbykey(boardkey).then(function (items) {
      //   console.log( "" + JSON.stringify(items[0]));
      //  var JsonObject= JSON.parse(JSON.stringify(items[0]));
      //  var vb = JsonObject.boardvalues;

      // if(vb == 2 ) 
      // {
      //    return res.render('errorpag');
      // } else {        
      //   res.render('boarddata', { "boards": list[0] ,  user: req.user });
      // }
      ///////// old

      database.getboardValuesandNamebykey(boardkey).then(function (items) {
        console.log( "" + JSON.stringify(items[0]));

       var n = JSON.stringify(items[0]);
       
       var JsonObject= JSON.parse(JSON.stringify(items[0]));
       var vb = JsonObject.boardvalues;
       var fullname = JsonObject.username[0].fullname;

      if(vb == 2 && fullname != req.user.fullname  ) 
      {
         return res.render('errorpag');
      } else {        

          //return res.send('you can not see this content 2' + "" + n + "" + fullname);

        res.render('boarddata', { "boards": items[0] ,  user: req.user });
      }


    });
      //else {
      //res.render('boarddata', { "boards": list[0] ,  user: req.user });
    },
      function (err) {
        console.error('problem ........', err);
        res.send("problem with the database. " + err);
      }


      );

    //}
    // else {
    //    return res.send('you can not see this content');
    // }
});


router.get('/:key/newnotes', function (req, res) {
  var boardkey = req.params.key;
  database.getboardbykey(boardkey)
    .then(function (items) {
      console.log(items);
      res.render('addnotes', { "boards": items[0] });
    },
      function (err) {
        console.error('problem ......', err);
        res.send("problem with the database. " + err);
      }

      );
});
router.post('/:key/addnotes', function (req, res) {
  var n_notes = {
    "boardkey": req.params.key,
    "notename": req.body.notename,
    "notes": req.body.notes
  };
   database.addnotes(n_notes)
      .then(
        function (result) { console.log(result); res.redirect("/boards"); },
        function (err) {
          console.error('problem ......', err);
          res.send("problem with the database. " + err);
        }
        );

});
module.exports = router;



function isLoggedIn(req, res, next) {  
  if (req.isAuthenticated())
      return next();
  res.redirect('/');
}

